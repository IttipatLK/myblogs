import React from 'react'
import Homepage from './pages/HomePage'
import BlogContentPage from './pages/BlogContentPage'
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import useFetch from './hooks/useFetch'
import RegisterPage from './pages/RegisterPage'
import Loginpage from './pages/LoginPage';
import Myblogpage from './pages/MyblogPage';
import EditBlogpage from './pages/EditBlogPage';
import CreateBlogpage from './pages/CreateBlogPage';

const API_GETBLOG = 'http://localhost:8080/blog/myblogs/getBlogs'

function App() {

  let {loading, data, error} = useFetch(API_GETBLOG)
  if(loading) return <p>Loading...</p>
  if(error) return <p>Error!</p>
  
  return (
    <Router>
    <div>
      <Routes>
        <Route path='/' element={<Homepage blogs={data?data:""} />}></Route>
        <Route path='/blog/:id' element={<BlogContentPage  blogs={data?data:""}/>}></Route>
        {/* <Route path='/myblog' element={<Myblogpage></Myblogpage>}></Route> */}
        {/* <Route path='/myblog' element={<Myblogpage blogs={data?data:""}/>}></Route> */}
        <Route path='/myblog' element={<Myblogpage/>}></Route>
        <Route path='/editBlog/:id' element={<EditBlogpage/>}></Route>
        <Route path='/register' element={<RegisterPage></RegisterPage>}></Route>
        <Route path='/login' element={<Loginpage></Loginpage>}></Route>
        <Route path='/createBlog' element={<CreateBlogpage></CreateBlogpage>}></Route>
        {/* <Route path='/pdf-viewer' element={<PdfPage></PdfPage>}></Route> */}
      </Routes>
    </div>
    </Router>
  )
}

export default App
