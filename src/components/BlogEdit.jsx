import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {Link, useNavigate, useParams} from 'react-router-dom';
import useFetchBearer from '../hooks/useFetchBearer'



const EditBlog = () => {

    const {id}=useParams()

    // const API_GETBLOGBID = `http://localhost:8002/blog/myblogs/getBlogId/${id}`
    // let {loading, data, error} = useFetchBearer(API_GETBLOGBID)


    // console.log("BID")
    // console.log(data)
    // if(loading) return <p>Loading...</p>
    // if(error) return <p>Error!</p>
    
    const navigateTo = useNavigate();

    const API_UPDATEBLOG = `http://localhost:8080/blog/myblogs/updateBlogBID/${id}`

    const [formData, setFormData] = useState({
        title: '',
        blogDesc: '',
        content: '',
        coverImage: '',
    });

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.put(API_UPDATEBLOG, formData,{
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            });
            alert(response.data.message);
            if (response.status === 200) {
                navigateTo('/myblog')
                window.location.reload();
            } else {
                console.error('Update Blog failed:', response.data.message);
            }
        } catch (error) {
            alert('Error Update Blog');
        }
    };

    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-100 space-x-20">
            {/* <div className="bg-white p-8 rounded-lg shadow-lg w-full max-w-md">
                <div  className='bg-white rounded-xl overflow-hidden drop-shadow-md'>
                        <img className='h-56 w-full object-cover' src="https://d2ouvy59p0dg6k.cloudfront.net/img/forest__hkun_lat_wwf___australia_691574.jpg" />
                        <div className='p-8'>
                            <h3 className='font-bold text-2xl my-1'>title</h3>
                            <p className='text-gray-600 text-xl'>description</p>
                        </div>
                </div>
                <h1 className="p-4 text-gray-600 font-bold text-2xl my-1">Let's introduce something new. Let's talk about your blog now.</h1>
            </div> */}
            <div className="bg-white p-8 rounded-lg shadow-lg w-full max-w-md">
                <div  className='bg-white rounded-xl overflow-hidden drop-shadow-md'>
                        <img className='h-56 w-full object-cover' src="https://cdn.britannica.com/70/234870-050-D4D024BB/Orange-colored-cat-yawns-displaying-teeth.jpg" />
                        <div className='p-8'>
                            <h3 className='font-bold text-2xl my-1'>Let's Edit !</h3>
                            <p className='text-gray-600 text-xl'>description</p>
                        </div>
                </div>
                <h1 className="p-4 text-gray-600 font-bold text-2xl my-1">Let's introduce something new. Let's talk about your blog now.</h1>
            </div>
            
            <div className="bg-white p-8 rounded-lg shadow-lg w-full max-w-md">
                <h2 className="text-2xl font-bold mb-6">Edit Blog</h2>
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label className="block text-gray-700">Title</label>
                        <input
                            type="text"
                            name="title"
                            value={formData.title}
                            onChange={handleChange}
                            // placeholder={data.data.title}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <div className="mb-4">
                        <label className="block text-gray-700">Description</label>
                        <textarea
                            type="text"
                            name="blogDesc"
                            value={formData.blogDesc}
                            onChange={handleChange}
                            // placeholder={data.data.blogDesc}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <div className="mb-4">
                        <label className="block text-gray-700">Content</label>
                        <textarea
                            type="text"
                            name="content"
                            value={formData.content}
                            onChange={handleChange}
                            // placeholder={data.data.content}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <div className="mb-4">
                        <label className="block text-gray-700">Image URL</label>
                        <input
                            type="text"
                            name="coverImage"
                            value={formData.coverImage}
                            onChange={handleChange}
                            // placeholder={data.data.coverImage}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <button
                        type="submit"
                        className="w-full bg-myblogbg text-white py-2 my-2 rounded-lg hover:opacity-[90%] transition duration-200"
                    >
                        Edit
                    </button>
                </form>
            </div>
        
        </div>
    );
};

export default EditBlog;
