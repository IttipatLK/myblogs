import { Link } from 'react-router-dom';
import DropDownBlog from './DropDownBlog';
import React, {useState} from 'react';
import { IoMenu } from "react-icons/io5";
import useFetchBearer from '../hooks/useFetchBearer'

// export default function BlogUser({blogs}) {
export default function BlogUser({token,userId}) {
    // console.log("Blog Object")
    // console.log(blogs)

    // const [openBlog,setOpenBlog] = useState(false)
    const [openBlog, setOpenBlog] = useState({});

    const toggleDropdown = (blogId) => {
        setOpenBlog((prevState) => ({
          ...prevState,
          [blogId]: !prevState[blogId]
        }));
    };

    const API_GETBLOGUID = `http://localhost:8080/blog/myblogs/getBlog/${userId}`
    let {loading, data, error} = useFetchBearer(API_GETBLOGUID)
    if(loading) return <p>Loading...</p>
    if(error) return <p>Error!</p>
    
    return (
        <div className='w-full bg-[#f9f9f9] py-[50px]'>
        <div className='max-w-[1240px] mx-auto'>
            <div className='grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2 ss:grid-cols-1 gap-8 px-4 text-black'>
            {data.data.map((blog)=>
                    <div  key={blog.blogId} className='bg-white rounded-xl overflow-hidden drop-shadow-md'>
                        <Link key={blog.blogId} to={`/blog/${blog.blogId}`}>
                            <div className='h-56 w-full object-cover'>
                                <img className='h-56 w-full object-cover' src={blog.coverImage} />
                            </div>
                        </Link>
                        {openBlog[blog.blogId] && <DropDownBlog blogId={blog.blogId}/>}

                        <Link onClick={() => toggleDropdown(blog.blogId)}>
                            <div className=' flex justify-end place-items-center'>
                            <h4 className='text-bold pr-5 pt-4'><IoMenu /></h4>
                            </div>
                        </Link>
                        <div className='px-8 pt-'>
                            <h3 className='font-bold text-2xl my-1'>{blog.title}</h3>
                            <p className='text-gray-600 text-xl'>{blog.blogDesc}</p>
                        </div>
                        <div className=' flex justify-end '>
                            <h1 className='font-semibold p-6  text-base text-gray-500 '>{blog.userName}</h1>
                        </div>
                    </div>
            )}
            </div>
        </div>
    </div>
        
    )
    
}
