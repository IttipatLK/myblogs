import React, { useState } from "react";
import axios from 'axios';
import {Link, useNavigate} from 'react-router-dom';



const DropDownBlog = ({blogId}) => {

    const API_DELETEBLOG = `http://localhost:8080/blog/myblogs/deleteBlog/${blogId}`

    const handleDeleteBlog = async () => {
        try {
    
            const response = await axios.delete(API_DELETEBLOG, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            });
            if (response.status === 200) {
                console.log("delete blog successfuly")
                window.location.reload();
            } else {
                console.error('Delete failed:', response.data.message);
            }
        } catch (error) {
            console.error('Error Delete failed', error);
        }
    };

    const navigateTo = useNavigate();

    const handleEdit = async () => {
        navigateTo(`/editBlog/${blogId}`);
    }

    return (
        <div className='flex flex-col dropDownBlog'>
            <ul className='flex flex-col'>
                <button className=" pt-4 pb-2 hover:opacity-[70%]" onClick={handleEdit} >✏️ Edit</button>
                <button className=" pt-2 pb-4 pt-0 hover:opacity-[70%]" onClick={() => handleDeleteBlog()}>🗑️ Delete</button>
            </ul>
        </div>
    )
}

export default DropDownBlog

