import React, { useState } from 'react';
import axios from 'axios';
import {Link, useNavigate} from 'react-router-dom';

const API_LOGIN = 'http://localhost:8080/user/myblogs/login'

const Login = () => {
    const navigateTo = useNavigate();

    const [formData, setFormData] = useState({
        username: '',
        password: '',
    });

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post(API_LOGIN, formData);
            alert(response.data.message);
            console.log("User Object")
            console.log(response.data)
            if (response.status === 200) {
                // เก็บ token ใน Local Storage
                localStorage.setItem('token', response.data.token);
                navigateTo('/')
            } else {
                console.error('Login failed:', response.data.message);
            }
        } catch (error) {
            alert('Error registering user');
        }
    };

    return (
        <div className="min-h-screen flex items-center justify-center bg-gray-100 space-x-20">
            <div className="bg-white p-8 rounded-lg shadow-lg w-full max-w-md">
                <div  className='bg-white rounded-xl overflow-hidden drop-shadow-md'>
                        <img className='h-56 w-full object-cover' src="https://i2-prod.ok.co.uk/incoming/article21758031.ece/ALTERNATES/s1227b/0_GettyImages-498178753.jpg" />
                        <div className='p-8'>
                            <h3 className='font-bold text-2xl my-1'>Have a good day ?</h3>
                            <p className='text-gray-600 text-xl'>Let's go have fun.</p>
                        </div>
                </div>
                <h1 className="p-4 text-gray-600 font-bold text-2xl my-1">Let's introduce something new. Let's talk about your blog now.</h1>
            </div>
            
            <div className="bg-white p-8 rounded-lg shadow-lg w-full max-w-md">
                <h2 className="text-2xl font-bold mb-6">Login</h2>
                <form onSubmit={handleSubmit}>
                    <div className="mb-4">
                        <label className="block text-gray-700">Username</label>
                        <input
                            type="text"
                            name="username"
                            value={formData.username}
                            onChange={handleChange}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <div className="mb-4">
                        <label className="block text-gray-700">Password</label>
                        <input
                            type="password"
                            name="password"
                            value={formData.password}
                            onChange={handleChange}
                            className="mt-1 p-2 w-full border border-gray-300 rounded"
                            required
                        />
                    </div>
                    <button
                        type="submit"
                        className="w-full bg-myblogbg text-white py-2 rounded-lg hover:opacity-[90%] transition duration-200"
                    >
                        Login
                    </button>
                    <h1 className='text-gray-600 pl-52 py-2'>don't have an account?</h1>
                    <Link to="/register">
                        <button
                            type="submit"
                            className="w-full bg-myblogbg text-white my-2 py-2 rounded-lg hover:opacity-[90%] transition duration-200"
                        >
                            Sign up
                        </button>
                    </Link>
                    
                </form>
            </div>
        
        </div>
    );
};

export default Login;
