import { Link } from 'react-router-dom';
import axios from 'axios';
import { IoIosCreate } from "react-icons/io";

const API_LOGOUT = 'http://localhost:8080/user/myblogs/logout'

export default function NavbarUser({name,userImage}) {
    return (
        <div className='w-full h-[80px] z-10 bg-white fixed drop-shadow-lg relative sticky top-0'>
            <div className='flex justify-between items-center w-full h-full md:max-w-[1240px] m-auto'>
                
                 
                <div className='flex items-center'>
                    {/* <img src="/src/assets/MyBlog_host.png" alt="logo" className='pl-5 sm:ml-10 ss:ml-10 md:ml-3 w-full h-[40px]' /> */}
                    {/* <Link to="/pdf-viewer">
                        <button className="flex items-center hover:opacity-[70%] rounded-md">
                            <img className='p-4 w-20 h-20 rounded-full' src={"https://i.ibb.co/8shVjrk/krebs.jpg"} />
                            <h1 className='font-bold text-1xl text-center text-myblogbg pt-0'>{"Ittipat lusuk"}</h1>
                        </button>
                    </Link> */}
                    <button className="flex items-center hover:opacity-[70%] rounded-md">
                            <img className='p-4 w-20 h-20 rounded-full' src={userImage} />
                            <h1 className='font-bold text-1xl text-center text-myblogbg pt-0'>{name}</h1>
                        </button>
                </div>


                <div className='hidden md:flex sm:mr-10 md:mr-10'>
                    {/* <img className='p-2 w-10 h-32 rounded-full mx-auto object-cover' src={"https://i.ibb.co/8shVjrk/krebs.jpg"} /> */}
                    <Link to="/">
                        <button className=" flex items-center hover:opacity-[70%] rounded-md">
                        <h1 className='pr-5 font-bold text-1xl text-center text-myblogbg pt-0'>{"Home"}</h1>
                        </button>
                    </Link>
                    <Link to="/myblog">
                        <button className=" flex items-center hover:opacity-[70%] rounded-md">
                        <h1 className='pr-5 font-bold text-1xl text-center text-myblogbg pt-0'>{"Myblog"}</h1>
                        </button>
                    </Link>
                    <Link to="/" onClick={handleLogout}>
                    <button className=" flex items-center hover:opacity-[70%] rounded-md">
                            <h1 className='pr-5 font-bold text-1xl text-center text-myblogbg pt-0'>{"Sign out"}</h1>
                    </button>
                    </Link>
                    <Link to="/createBlog" >
                    <button className=" flex items-center hover:opacity-[70%] rounded-md">
                            <h1 className='pr-5 font-bold text-2xl text-center text-myblogbg pt-0'>{<IoIosCreate />}</h1>
                    </button>
                    </Link>
                    {/* <Link to="/pdf-viewer">
                        <button className=" flex items-center hover:opacity-[70%] rounded-md">
                            <h1 className='pr-5 font-bold text-1xl text-center text-myblogbg pt-0'>{"About us"}</h1>
                        </button>
                    </Link> */}
                    {/* <a href="https://github.com/Experzir" className="hover:opacity-[70%] rounded-md font-bold text-1xl text-center text-myblogbg pt-0">{"Github 📤"}</a> */}
                    {/* <Link to="/pdf-viewer">
                        <button className="px-3 py-1 flex items-center border hover:opacity-[70%] rounded-md bg-myblogbg">
                            <h1 className='font-semibold text-1xl text-center text-white pt-0'>{"Login/Sign up"}</h1>
                        </button>
                    </Link> */}
                    
                </div>

            </div>
        </div>
        
    )
}

// const handleLogout = () => {
//     localStorage.removeItem('token');
//     window.location.reload();
// };

const handleLogout = async () => {
    try {
        // ส่ง request ไปยัง API เพื่อลบ token
        const response = await axios.post(API_LOGOUT, null, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        });
        localStorage.removeItem('token');
        // navigateTo('/');
        window.location.reload();
    } catch (error) {
        console.error('Error logging out:', error);
    }
};




