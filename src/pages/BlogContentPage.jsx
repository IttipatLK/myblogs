import Navbar  from '../components/Navbar'
import BlogContent from '../components/BlogContent'
import Footer from '../components/Footer'
import React from 'react'
import {useEffect, useState} from 'react';
import { jwtDecode } from "jwt-decode";
import NavbarUser from '../components/NavbarUser';

function BlogContentPage({blogs}) {
  console.log(blogs)
  const [userData, setUserData] = useState({
    userId: '',
    name: '',
    userImage: "",
    token:''
  });

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = jwtDecode(token);
      // console.log('Decoded token:', decoded);
      console.log('token:', token);

      const currentTime = Math.floor(Date.now() / 1000); // Get current time in Unix timestamp
      if (decoded.exp < currentTime) {
        // Token has expired
        localStorage.removeItem('token'); // Remove expired token
        navigateTo('/'); // Redirect to home page
      } else {
        const userId = decoded.userId;
        const name = decoded.name;
        const userImage = decoded.userImage;
        console.log('User ID from token:', userId);
        console.log('User name from token:', name);
        console.log('User Image from token:', userImage);

        // Update state with user data
        setUserData({
          userId,
          name,
          userImage,
          token
        });
      }
    } 
  }, []); // Run only once on component mount

  if (!userData.token) {
    return (
      <div>
        <Navbar />
        <BlogContent blogs={blogs}/>   
        <Footer />
      </div> 
    )
  } else {
    return (
      <div>
        <NavbarUser name={userData.name} userImage={userData.userImage} />
        <BlogContent blogs={blogs}/>   
        <Footer />
      </div> 
    )
  }
  
}

export default BlogContentPage