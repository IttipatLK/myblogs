import Navbar  from '../components/Navbar'
import Blogs from '../components/Blogs'
import Footer from '../components/Footer'
import React from 'react'
import NavbarUser from '../components/NavbarUser'
import {useEffect, useState} from 'react';
import { jwtDecode } from "jwt-decode";
import {useNavigate} from 'react-router-dom';
import BlogUser from '../components/BlogUser'
import Loginpage from './LoginPage'
import CreateBlog from '../components/BlogCreate'

function CreateBlogpage() {
  const navigateTo = useNavigate();

  const [userData, setUserData] = useState({
    userId: '',
    name: '',
    userImage: "",
    token:''
  });

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      const decoded = jwtDecode(token);
      // console.log('Decoded token:', decoded);
      console.log('token:', token);

      const currentTime = Math.floor(Date.now() / 1000); // Get current time in Unix timestamp
      if (decoded.exp < currentTime) {
        // Token has expired
        localStorage.removeItem('token'); // Remove expired token
        navigateTo('/'); // Redirect to home page
      } else {
        const userId = decoded.userId;
        const name = decoded.name;
        const userImage = decoded.userImage;
        console.log('User ID from token:', userId);
        console.log('User name from token:', name);
        console.log('User Image from token:', userImage);

        // Update state with user data
        setUserData({
          userId,
          name,
          userImage,
          token
        });
      }
    } 
  }, []);

  if (!userData.token) {
    return (
      <div>
        <Loginpage></Loginpage>
      </div> 
    )
  } else {
    return (
      <div>
        <NavbarUser name={userData.name} userImage={userData.userImage} />
        <CreateBlog userId={userData.userId} />
        <Footer />
      </div> 
    )
  }
  
}

export default CreateBlogpage
