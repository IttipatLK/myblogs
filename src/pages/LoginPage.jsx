import Navbar  from '../components/Navbar'
import Blogs from '../components/Blogs'
import Footer from '../components/Footer'
import React from 'react'
import Login from '../components/Login'


function Loginpage() {
    return (
        <div>
          <Navbar />
          <Login/>
          <Footer />
        </div> 
    )
  }
  
  export default Loginpage