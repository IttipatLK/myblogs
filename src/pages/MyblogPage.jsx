import Navbar  from '../components/Navbar'
import Blogs from '../components/Blogs'
import Footer from '../components/Footer'
import React from 'react'
import NavbarUser from '../components/NavbarUser'
import {useEffect, useState} from 'react';
import { jwtDecode } from "jwt-decode";
import {useNavigate} from 'react-router-dom';
import BlogUser from '../components/BlogUser'
import Loginpage from './LoginPage'


function Myblogpage() {
  const navigateTo = useNavigate();

  const [userData, setUserData] = useState({
    userId: '',
    name: '',
    userImage: "",
    token:''
  });

  // const [API_GETBLOGBID, setAPI_GETBLOGBID] = useState(null);

  
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) { 
      try {
        const decoded = jwtDecode(token);
        console.log('token:', token);

        const currentTime = Math.floor(Date.now() / 1000); 
        if (decoded.exp < currentTime) {
          // Token has expired
          localStorage.removeItem('token'); // Remove expired token
          navigateTo('/'); // Redirect to home page
        } else {
          const { userId, name, userImage } = decoded;
          console.log('User ID from token:', userId);
          console.log('User name from token:', name);
          console.log('User Image from token:', userImage);

          // Update state with user data
          setUserData({
            userId,
            name,
            userImage,
            token
          });

          // setAPI_GETBLOGBID(`http://localhost:8002/blog/myblogs/getBlog/${userId}`);

        }
      } catch (error) {
        console.log('Invalid token:', error);
        localStorage.removeItem('token'); 
        navigateTo('/'); 
      }
    } else {
      localStorage.removeItem('token'); 
      navigateTo('/'); 
    }
  }, [navigateTo]);

  
  // let {loading, data, error} = useFetchBearer(API_GETBLOGBID)
  // console.log("data")
  // console.log(data)
  // if(loading) return <p>Loading...</p>
  // if(error) return <p>Error!</p>

  if (!userData.token) {
    return (
      <div>
        <Loginpage></Loginpage>
      </div>    
    )
  } else {
    return (
      <div>
        <NavbarUser name={userData.name} userImage={userData.userImage} />
        {/* <BlogUser blogs={data?data:""} />     */}
        <BlogUser token={userData.token} userId={userData.userId} />   
        <Footer />
      </div> 
    )
  }
  
}

export default Myblogpage
