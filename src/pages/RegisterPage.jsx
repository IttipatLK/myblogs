import Navbar  from '../components/Navbar'
import Footer from '../components/Footer'
import React from 'react'
import Register from '../components/Register'



function Registerpage() {
    return (
        <div>
          <Navbar />
          <Register/>
          <Footer />
        </div> 
    )
  }
  
  export default Registerpage